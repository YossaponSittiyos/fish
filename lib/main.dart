import 'package:flutter/material.dart';
import "page/calculate.dart";
import "page/about.dart";

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          errorColor: const Color.fromARGB(255, 255, 0, 0),
          applyElevationOverlayColor: true,
          appBarTheme: const AppBarTheme(
            backgroundColor: Color(0xFF2987be),
            titleTextStyle: TextStyle(color: Color(0xFF000000)),
            iconTheme: IconThemeData(color: Color(0xFF000000)),
          ),
          fontFamily: 'Sukhumvit Set Text',
          inputDecorationTheme: const InputDecorationTheme(
            labelStyle: TextStyle(),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFF461f00), width: 2.0),
            ),
          )),
      home: const MyHomePage(),
      initialRoute: '/',
      routes: {
        '/calculate': (context) => const Calculate(),
        '/about': (context) => const About(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      backgroundColor: const Color(0xFFeeeeee),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height * 0.45,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage('assets/images/logo_app2.png'),
                        fit: BoxFit.fitHeight),
                  )),
              SizedBox(
                height: 200,
                width: MediaQuery.of(context).size.width,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/calculate');
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Color(0xFF2b5288),
                                  Color(0xFF2987be),
                                ],
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Container(
                              height: 65,
                              constraints: const BoxConstraints(
                                  minWidth: 88.0,
                                  minHeight:
                                      36.0), // min sizes for Material buttons
                              alignment: Alignment.center,
                              child: const Text(
                                "คำนวณปริมาณการใช้ฮอร์โมนสังเคราะห์",
                                style: TextStyle(
                                    color: Color(0xFFeeeeee),
                                    fontWeight: FontWeight.w600,
                                    fontSize: 22),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/about');
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          padding: const EdgeInsets.all(0.0),
                          child: Ink(
                            decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Color(0xFFd2d611),
                                  Color(0xFF597c29),
                                ],
                              ),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Container(
                              height: 60,
                              constraints: const BoxConstraints(
                                  minWidth: 88.0,
                                  minHeight:
                                      36.0), // min sizes for Material buttons
                              alignment: Alignment.center,
                              child: const Text(
                                "คำอธิบาย",
                                style: TextStyle(
                                    color: Color(0xFFeeeeee),
                                    fontWeight: FontWeight.w600,
                                    fontSize: 22),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
