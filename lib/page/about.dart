import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFffffff),
        title: const Text('คำอธิบาย',
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            MyCardInfo(
              number: '01',
              icon: Icons.app_shortcut_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 255, 238, 82)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 255, 230, 0)
              ],
              textTitle: 'รายละเอียด',
              detail:
                  "  โปรแกรมนี้ใช้ช่วยคำนวณฮอร์โมนสังเคราะห์ Buserelin acetate (Luteinizing Hormone-Releasing Hormone analogue: LHRHa) มีชื่อทางการค้าได้แก่ สุพรีแฟกท์ (Suprefact) และ ซินนาแฟค อี (Cinnafact  E) ใช้สำหรับเพาะพันธุ์สัตว์น้ำ ทั้งนี้ผู้ที่จะใช้โปรแกรมนี้ควรมีความรู้พื้นฐานด้านการเพาะพันธุ์สัตว์น้ำด้วยฮอร์โมนสังเคราะห์มาก่อน ",
            ),
            MyCardInfo(
              number: '02',
              icon: Icons.business_center_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 255, 174, 82)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 255, 136, 0)
              ],
              textTitle: 'คำแนะนำ',
              detail:
                  " สำหรับการเลือกใช้ปริมาณสารละลายฮอร์โมนรวม (ข้อ4)	สัตว์น้ำมีน้ำหนักเฉลี่ยต่อตัวในช่วงระหว่าง 0.20 - 5.00  กิโลกรัม  แนะนำให้ใช้สารละลายฮอร์โมนรวม 1 ซี.ซี./ก.ก สัตว์น้ำมีน้ำหนักเฉลี่ยต่อตัวมากกว่า 5 กิโลกรัม  แนะนำให้ใช้สารละลายฮอร์โมนรวม 0.5 ซี.ซี./ก.ก.",
            ),
            MyCardInfo(
              number: '03',
              icon: Icons.auto_awesome_mosaic_rounded,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 229, 255, 82)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 217, 255, 0)
              ],
              textTitle: 'อัตราส่วน',
              detail:
                  " การฉีดฮอร์โมนเพื่อเพาะพันธุ์สัตว์น้ำด้วยโปรแกรมนี้จะต้องสร้างสารละลายที่เจือจาง (Dilute Solution) ของฮอร์โมนสังเคราะห์ให้มีความเข้มข้น 1,000 ไมโครกรัม/ซี.ซี. หรือ 100 ไมโครกรัม/ซี.ซี. ไว้ก่อน ที่จะทำการเพาะพันธุ์สัตว์น้ำ ",
            ),
            MyCardInfo(
              number: '04',
              icon: Icons.bubble_chart_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 122, 255, 82)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 60, 255, 0)
              ],
              textTitle: 'สารละลายฮอร์โมน',
              detail:
                  " สารละลายฮอร์โมนรวม หมายถึงสารละลายที่เกิดจากการผสมระหว่าง ฮอร์โมนสังเคราะห์ รวมกับยาเสริมฤทธิ์ รวมกับน้ำกลั่น",
            ),
            MyCardInfo(
              number: '05',
              icon: Icons.album_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 82, 243, 255)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 0, 238, 255)
              ],
              textTitle: 'การเพาะพันธุ์',
              detail:
                  " การเพาะพันธุ์สัตว์น้ำด้วยด้วยการฉีดฮอร์โมนสังเคราะห์ควรทำการชั่งน้ำหนักทีละเพศ ",
            ),
            MyCardInfo(
              number: '06',
              icon: Icons.local_hospital_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 82, 128, 255)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 0, 68, 255)
              ],
              textTitle: 'การแบ่งกลุ่มสัตว์น้ำ',
              detail:
                  " กรณีที่ต้องการเพาะพันธุ์สัตว์น้ำจำนวนมาก ควรทำการแบ่งกลุ่มสัตว์น้ำเป็นกลุ่มตามน้ำหนักตัวให้มีขนาดใกล้เคียงกัน จากนั้นจึงทำการชั่งน้ำหนักรวมของปลาแต่ละกลุ่ม เพื่อลดความผิดพลาดที่เกิดจากขนาดตัวแตกต่างกันมากๆ ",
            ),
            MyCardInfo(
              number: '07',
              icon: Icons.webhook_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 157, 82, 255)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 111, 0, 255)
              ],
              textTitle: 'สารละลายฮอร์โมน',
              detail:
                  " การเพาะสัตว์น้ำด้วยฮอร์โมนสังเคราะห์ โดยทั่วไปควรใช้ฮอร์โมนสังเคราะห์ในปริมาณ 5 - 40 ไมโครกรัม/น้ำหนักสัตว์น้ำ 1 กิโลกรัม ทั้งนี้ขึ้นอยู่กับชนิดสัตว์น้ำ หรือความสมบูรณ์ของไข่และน้ำเชื้อของสัตว์น้ำ รวมถึงความชำนาญของผู้เพาะพันธุ์แต่ละคน  แต่การใช้ฮอร์โมนในปริมาณที่สูงเกินไปอาจส่งผลให้เกิดการยับยั้งการวางไข่ของสัตว์น้ำได้ ",
            ),
            MyCardInfo(
              number: '08',
              icon: Icons.storm_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 241, 82, 255)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 234, 0, 255)
              ],
              textTitle: 'ยาเสริมฤทธิ์',
              detail:
                  "  ยาเสริมฤทธิ์ เป็นยาที่มีสารออกฤทธิ์คือ Domperidone Maleate มีชื่อทางการค้าหลายชนิด ได้แก่ Motilium-M หรือ Molax-M หรือ Motidom-M ผู้ใช้สามารถเลือกใช้ได้ตามความเหมาะสม ก่อนใช้งานต้องบดเม็ดยาด้วยโกร่งบดยาให้เป็นผงละเอียด เพื่อป้องกันการอุดตันเวลาใช้เข็มฉีดยาดูดสารละลายฮอร์โมนรวม ",
            ),
            MyCardInfo(
              number: '09',
              icon: Icons.water_drop_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 255, 82, 183)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 255, 0, 149)
              ],
              textTitle: 'สารออกฤทธิ์',
              detail:
                  " ยาเสริมฤทธิ์ 1 เม็ด มีสารออกฤทธิ์คือ Domperidone maleate จำนวน 10 มิลลิกรัม การเพาะพันธุ์สัตว์น้ำโดยทั่วไปจะใช้ยาเสริมฤทธิ์ จำนวน 1 เม็ดต่อน้ำหนักสัตว์น้ำ 1 กิโลกรัม",
            ),
            MyCardInfo(
              number: '10',
              icon: Icons.sticky_note_2_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 255, 82, 82)
              ],
              borderColor: const [Colors.black, Color.fromARGB(255, 255, 0, 0)],
              textTitle: 'วิธีการผสม',
              detail:
                  " วิธีการผสมสารละลายฮอร์โมนรวม เริ่มจากบดยาเสริมฤทธิ์ด้วยโกร่งบดยาจนเป็นผงละเอียด จากนั้นใช้เข็มและกระบอกฉีดยา (ใหม่) ดูดฮอร์โมนสังเคราะห์จากขวดสารละลายที่เจือจางแล้วมาผสมกับผงยาเสริฒฤทธิ์ จากนั้นเติมน้ำกลั่นลงไปและคนให้ผสมเข้ากันให้ทั่ว จะได้สารละลายฮอร์โมนรวม ที่พร้อมสำหรับนำไปฉีดให้กับพ่อ-แม่พันธุ์สัตว์น้ำ",
            ),
            MyCardInfo(
              number: '11',
              icon: Icons.science_outlined,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 255, 238, 82)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 255, 230, 0)
              ],
              textTitle: 'ฮอร์โมนสังเคราะห์',
              detail:
                  " ฮอร์โมนสังเคราะห์ควรเก็บไว้ในตู้เย็น (ในช่องปกติ) และไม่ควรให้ฮอร์โมนถูกแสงแดดและความร้อน เนื่องจากจะทำให้ฮอร์โมนเสื่อมสภาพ",
            ),
            MyCardInfo(
              number: '12',
              icon: Icons.vaccines_rounded,
              bgCardNumber: const [
                Colors.black,
                Color.fromARGB(255, 255, 174, 82)
              ],
              borderColor: const [
                Colors.black,
                Color.fromARGB(255, 255, 136, 0)
              ],
              textTitle: 'ข้อควรระวัง',
              detail:
                  " การดูฮอร์โมนสังเคราะห์ออกจากขวดควรใช้เข็มฉีดยา และกระบอกฉีดยาอันใหม่เสมอ เพื่อเป็นการป้องกันการปนเบื้อนเชื้อโรคจากเข็มฉีดยา และกระบอกฉีดยาที่ใช้จุ่มในขวดฮอร์โมน ซึ่งเป็นสาเหตุของการเสื่อมสภาพของฮอร์โมน ",
            ),
          ],
        ),
      ),
    );
  }
}

class MyCardInfo extends StatelessWidget {
  MyCardInfo({
    Key? key,
    this.number = '',
    this.textTitle = '',
    this.detail = '',
    required this.icon,
    required this.borderColor,
    required this.bgCardNumber,
  }) : super(key: key);

  final String? number;
  final String? textTitle;
  final String detail;
  final IconData? icon;
  final List<Color> borderColor;
  final List<Color> bgCardNumber;

  final kInnerDecoration = BoxDecoration(
    color: Colors.white,
    border: Border.all(color: Color.fromARGB(255, 0, 0, 0)),
    borderRadius: BorderRadius.circular(10),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8),
      // height: 66.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: borderColor,
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.8),
            spreadRadius: 4,
            blurRadius: 5,
            offset: const Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Stack(
          children: <Widget>[
            Container(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 12, top: 70),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10, right: 16),
                            child: Text(
                              '$textTitle',
                              style: const TextStyle(
                                fontSize: 36,
                                shadows: [
                                  Shadow(
                                      color: Color.fromARGB(255, 70, 70, 70),
                                      offset: Offset(0, -10))
                                ],
                                color: Colors.transparent,
                                decoration: TextDecoration.underline,
                                decorationColor:
                                    Color.fromARGB(255, 218, 218, 218),
                                decorationThickness: 2,
                              ),
                            ),
                          ),
                          Icon(
                            icon,
                            color: const Color.fromARGB(255, 70, 70, 70),
                            size: 40.0,
                          ),
                        ],
                      ),
                    ),
                    Text(
                      detail,
                      style: const TextStyle(fontSize: 24),
                    ),
                  ],
                ),
              ),
              decoration: kInnerDecoration,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                width: 60,
                height: 70,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: bgCardNumber,
                )),
                child: Center(
                  child: Text(
                    '$number',
                    style: const TextStyle(
                      fontSize: 36.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
