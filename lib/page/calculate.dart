import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:reactive_forms/reactive_forms.dart';

class Calculate extends StatefulWidget {
  const Calculate({Key? key}) : super(key: key);

  @override
  State<Calculate> createState() => _CalculateState();
}

class _CalculateState extends State<Calculate> {
  final form = FormGroup({
    'totalWeightAnimals':
        FormControl<String>(value: '', validators: [Validators.required]),
    'synergisticDrugs':
        FormControl<String>(value: '', validators: [Validators.required]),
    'suprefact': FormControl(value: '', validators: [Validators.required]),
    'hormone': FormControl(value: '', validators: [Validators.required]),
    'amount': FormControl(value: '', validators: [Validators.required]),
  });

  _onSubmit(data) {
    calculate(data);
  }

  StreamController<double> totalWeightAnimals = StreamController<double>();

  StreamController<double> synergisticDrugs = StreamController<double>();

  StreamController<double> suprefact = StreamController<double>();

  StreamController<double> hormone = StreamController<double>();

  StreamController<double> amount = StreamController<double>();

  var b6 = 0.00;

  var b7 = 0.00;

  var b8 = 0.00;

  var b9 = 0.00;

  var b10 = 0.00;

  var b11 = 0.00;

  var b12 = 0.00;

  var b13 = 0.00;

  var b14 = 0.00;

  var b15 = 0.00;

  calculate(data) {
    FocusManager.instance.primaryFocus?.unfocus();

    b6 = double.parse(data['totalWeightAnimals']);
    b7 = double.parse(data['synergisticDrugs']);
    b8 = double.parse(data['suprefact']);
    b9 = double.parse(data['hormone']);
    b10 = double.parse(data['amount']);

    b11 = b6 * b7;
    b12 = ((b6 * b8) * 1) / 100;
    b13 = (b6 * b9) - b12;
    b14 = b12 + b13;
    b15 = b6 / b10;

    totalWeightAnimals.add(double.parse(b11.toStringAsFixed(2)));
    synergisticDrugs.add(double.parse(b12.toStringAsFixed(2)));
    suprefact.add(double.parse(b13.toStringAsFixed(2)));
    hormone.add(double.parse(b14.toStringAsFixed(2)));
    amount.add(double.parse(b15.toStringAsFixed(2)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xFFeeeeee),
        appBar: AppBar(
          title: const Text('คำนวณปริมาณการใช้ฮอร์โมน',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
        ),
        body: Scrollbar(
          child: ListView(
            children: [
              ReactiveForm(
                formGroup: form,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Padding(
                          padding: EdgeInsets.only(
                              left: 36, right: 36, top: 25, bottom: 15),
                          child: Text(
                            'ป้อนตัวเลขที่ต้องการคำนวณลงไปในช่องด้านล่าง',
                            style: TextStyle(
                                color: Color.fromARGB(255, 0, 0, 0),
                                fontSize: 24,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          )),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                        child: ReactiveTextField(
                          formControlName: 'totalWeightAnimals',
                          validationMessages: (control) => {
                            'required':
                                'กรุณาเพื่มน้ำหนักรวมของสัตว์น้ำทั้งหมดที่จะฉีดฮอร์โมน'
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^\d*\.?\d{0,2}'))
                          ],
                          autofocus: false,
                          textCapitalization: TextCapitalization.words,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelStyle: const TextStyle(
                              color: Color(0xFF461f00),
                              letterSpacing: 1.3,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                            label: const Text(
                              '(1) น้ำหนักรวมของสัตว์น้ำทั้งหมดที่จะฉีดฮอร์โมน',
                              style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                height: 1.2
                              )
                            ),
                            suffix: const Text(
                              '(ก.ก.)',
                              style: TextStyle(color: Color(0xFF461f00)),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0xFF461f00),
                                  width: 2.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                        child: ReactiveTextField(
                          formControlName: 'synergisticDrugs',
                          validationMessages: (control) => {
                            'required':
                                'กรุณาเพื่มปริมาณยาเสริมฤทธิ์ต่อน้ำหนักสัตว์น้ำ',
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^\d*\.?\d{0,2}'))
                          ],
                          autofocus: false,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelStyle: const TextStyle(
                              color: Color(0xFF461f00),
                              letterSpacing: 1.3,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                            ),
                            suffix: const Text(
                              '(เม็ด/ก.ก.)',
                              style: TextStyle(color: Color(0xFF461f00)),
                            ),
                            label: const Text(
                              '(2) ปริมาณยาเสริมฤทธิ์ต่อน้ำหนักสัตว์น้ำ',style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                height: 1.2
                              )
                            ),
                            fillColor: Colors.white,
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0xFF461f00),
                                  width: 2.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                        child: ReactiveTextField(
                          formControlName: 'suprefact',
                          validationMessages: (control) => {
                            'required':
                                'กรุณาเพื่มปริมาณการใช้ฮอร์โมนสังเคราะห์ต่อน้ำหนักสัตว์น้ำ',
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^\d*\.?\d{0,2}'))
                          ],
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelStyle: const TextStyle(
                              color: Color(0xFF461f00),
                              letterSpacing: 1.3,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                            ),
                            suffix: const Text(
                              '(ไมโครกรัม/ก.ก.)',
                              style: TextStyle(color: Color(0xFF461f00)),
                            ),
                            label: const Text(
                              "(3) ปริมาณการใช้ฮอร์โมนสังเคราะห์ต่อน้ำหนักสัตว์น้ำ",
                              style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                height: 1.2
                              )
                            ),
                            fillColor: Colors.white,
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0xFF461f00),
                                  width: 2.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                        child: ReactiveTextField(
                          // cursorHeight: 25,
                          strutStyle: const StrutStyle(
                            height: 2.5
                          ),
                          formControlName: 'hormone',
                          validationMessages: (control) => {
                            'required':
                                'กรุณาปริมาณสารละลายฮอร์โมนรวม ที่ต้องการใช้ฉีดต่อน้ำหนักเฉลี่ยต่อตัวของสัตว์น้ำ',
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^\d*\.?\d{0,2}'))
                          ],
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelStyle: const TextStyle(
                              color: Color(0xFF461f00),
                              letterSpacing: 1.3,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                            ),
                            suffix: const Text(
                              '(ซี.ซี./ก.ก.)',
                              style: TextStyle(color: Color(0xFF461f00)),
                            ),
                            label: const Text(
                              "(4) ปริมาณสารละลายฮอร์โมนรวม ที่ต้องการใช้ฉีดต่อน้ำหนักเฉลี่ยต่อตัวของสัตว์น้ำ",
                              maxLines: 2,
                              style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                height: 1.2
                              )
                            ),
                            fillColor: Colors.white,
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0xFF461f00),
                                  width: 2.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                        child: ReactiveTextField(
                          formControlName: 'amount',
                          validationMessages: (control) => {
                            'required': 'กรุณาเพื่มจำนวนสัตว์น้ำทั้งหมดที่ชั่งน้ำหนักรวม',
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^\d*\.?\d{0,2}'))
                          ],
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            labelStyle: const TextStyle(
                              color: Color(0xFF461f00),
                              letterSpacing: 1.3,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                            ),
                            suffix: const Text(
                              '(ตัว)',
                              style: TextStyle(color: Color(0xFF461f00)),
                            ),
                            label: const Text(
                              "(5) จำนวนสัตว์น้ำทั้งหมดที่ชั่งน้ำหนักรวม",
                              style: TextStyle(
                                overflow: TextOverflow.ellipsis,
                                height: 1.2
                              )
                            ),
                            fillColor: Colors.white,
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Color(0xFF461f00),
                                  width: 2.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                            child: ReactiveFormConsumer(
                              builder: (context, form, child) {
                                return ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      fixedSize: const Size(150, 50),
                                      primary: const Color(0xFF515655), // background
                                      shadowColor: const Color.fromARGB(
                                          255, 51, 69, 107), // shadow
                                    ),
                                    child: const Text(
                                      'ล้างข้อมูล',
                                      style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.w600,
                                          color: Color.fromARGB(
                                              255, 255, 255, 255)),
                                    ),
                                    onPressed: () {
                                      form.resetState({
                                        'totalWeightAnimals':
                                            ControlState(value: ''),
                                        'synergisticDrugs':
                                            ControlState(value: ''),
                                        'suprefact': ControlState(value: ''),
                                        'hormone': ControlState(value: ''),
                                        'amount': ControlState(value: ''),
                                      });
                                      form.clearValidators();
                                      totalWeightAnimals.add(0.00);
                                      synergisticDrugs.add(0.00);
                                      suprefact.add(0.00);
                                      hormone.add(0.00);
                                      amount.add(0.00);
                                    });
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                            child: ReactiveFormConsumer(
                              builder: (context, form, child) {
                                return ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    fixedSize: const Size(180, 50),
                                    primary: const Color(0xFF2987be), // background
                                    onPrimary: const Color.fromARGB(
                                        255, 0, 0, 0), // foreground
                                    shadowColor: const Color(0xFF213158), // shadow
                                  ),
                                  child: const Text(
                                    'คำนวณผลลัพธ์',
                                    style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFFeeeeee),
                                    ),
                                  ),
                                  onPressed: form.valid
                                      ? () {
                                          _onSubmit(form.value);
                                        }
                                      : null,
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8, 18, 8, 10),
                        child: Column(
                          children: [
                            const Align(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                                padding: EdgeInsets.only(bottom: 16),
                                child: Text(
                                  "ผลการคำนวณ",
                                  style: TextStyle(
                                      color: Color.fromRGBO(0, 0, 0, 1),
                                      fontSize: 24,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                            StreamBuilder<double>(
                                stream: totalWeightAnimals.stream,
                                builder: (context, snapshot) {
                                  return snapshot.hasData &&
                                          snapshot.data != 0.00
                                      ? MyTexteResource(
                                          textTitle:
                                              '(1) ปริมาณยาเสริมฤทธิ์(Motilium-M)ทั้งหมดที่ใช้',
                                          result: snapshot.data,
                                          suffix: '(เม็ด)')
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: const <Widget>[
                                            Icon(
                                              Icons.close_rounded,
                                              color: Colors.red,
                                              size: 24.0,
                                            ),
                                            Text(
                                              'ไม่พบผลลัพธ์คำนวณปริมาณการใช้ฮอร์โมน',
                                              style: TextStyle(
                                                  color: Color.fromARGB(
                                                      255, 255, 0, 0),
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Icon(
                                              Icons.priority_high_rounded,
                                              color: Colors.red,
                                              size: 24.0,
                                            ),
                                          ],
                                        );
                                }),
                            StreamBuilder<double>(
                                stream: synergisticDrugs.stream,
                                builder: (context, snapshot) {
                                  return snapshot.hasData &&
                                          snapshot.data != 0.00
                                      ? MyTexteResource(
                                          textTitle:
                                              '(2) ปริมาณซูปรีแฟคที่ต้องใช้ (จากขวดสารละลายตั้งต้น)',
                                          result: snapshot.data,
                                          suffix: '(ซี.ซี.)')
                                      : const SizedBox();
                                }),
                            StreamBuilder<double>(
                                stream: suprefact.stream,
                                builder: (context, snapshot) {
                                  return snapshot.hasData &&
                                          snapshot.data != 0.00
                                      ? MyTexteResource(
                                          textTitle:
                                              '(3) ปริมาณน้ำกลั่นที่ต้องใช้เป็นส่วนผสมในสารละลายฮอร์โมน',
                                          result: snapshot.data,
                                          suffix: '(ซี.ซี.)')
                                      : const SizedBox();
                                }),
                            StreamBuilder<double>(
                                stream: hormone.stream,
                                builder: (context, snapshot) {
                                  return snapshot.hasData &&
                                          snapshot.data != 0.00
                                      ? MyTexteResource(
                                          textTitle:
                                              '(4) ปริมาณของสารละลายฮอร์โมนทั้งหมดที่ได้',
                                          result: snapshot.data,
                                          suffix: '(ซี.ซี.)')
                                      : const SizedBox();
                                }),
                            StreamBuilder<double>(
                                stream: amount.stream,
                                builder: (context, snapshot) {
                                  return snapshot.hasData &&
                                          snapshot.data != 0.00
                                      ? MyTexteResource(
                                          textTitle:
                                              ' (5) ปริมาณสารละลายฮอร์โมนที่ฉีดแต่ละตัว',
                                          result: snapshot.data,
                                          suffix: '(ซี.ซี.)')
                                      : const SizedBox();
                                }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

class MyTexteResource extends StatelessWidget {
  const MyTexteResource(
      {Key? key, this.textTitle = '', this.result = 0.00, this.suffix = ''})
      : super(key: key);

  final String textTitle;
  final double? result;
  final String suffix;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            textTitle,
            style: const TextStyle(
                color: Color.fromARGB(255, 0, 8, 85),
                fontSize: 18,
                fontWeight: FontWeight.w400),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "$result  $suffix",
              style: const TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 1),
                  fontSize: 24,
                  fontWeight: FontWeight.w800),
            ),
          ),
        ),
        const Divider(
          color: Color.fromARGB(255, 0, 14, 136),
        )
      ],
    );
  }
}
